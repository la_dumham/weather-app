import 'package:flutter/material.dart';
import 'package:weather_app/routes/route_name.dart';
import 'package:weather_app/view/home.dart';

class Routes {
  static String initial = SLASH;

  static final paths = <String, WidgetBuilder>{
    HOME: (BuildContext context) => const Home(),
  };
}
