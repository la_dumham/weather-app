import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart';
import 'package:weather_app/controller/get_data.dart';
import 'package:weather_app/routes/route.dart';
import 'package:weather_app/utils/color_me.dart';
import 'package:weather_app/view/slash/splashscreen.dart';

void main() {
  runApp(
    MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => GetData()),
      ],
      child: const MyApp(),
    ),
  );
}

class MyBehavior extends ScrollBehavior {
  @override
  Widget buildViewportChrome(
      BuildContext context, Widget child, AxisDirection axisDirection) {
    return child;
  }
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
      designSize: const Size(360, 690),
      minTextAdapt: true,
      splitScreenMode: true,
      builder: (_) {
        return MaterialApp(
            builder: (context, child) {
              return ScrollConfiguration(
                behavior: MyBehavior(),
                child: Container(child: child),
              );
            },
            debugShowCheckedModeBanner: false,
            // Use this line to prevent extra rebuilds
            useInheritedMediaQuery: true,
            title: 'First Method',
            // You can use the library anywhere in the app even in theme
            theme: ThemeData(
              iconTheme: IconThemeData(color: Color_me.main),
              textSelectionTheme: const TextSelectionThemeData(
                cursorColor: Colors.black, //thereby
              ),
              primaryColor: Colors.white,
              scaffoldBackgroundColor: Colors.white,
              appBarTheme: AppBarTheme(color: Color_me.main),
              visualDensity: VisualDensity.adaptivePlatformDensity,
              errorColor: Colors.red,
              checkboxTheme: const CheckboxThemeData(),
            ),
            home: const SplashScreen(),
            routes: Routes.paths);
      },
    );
  }
}
