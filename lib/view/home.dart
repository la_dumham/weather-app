import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:weather_app/controller/get_data.dart';
import 'package:weather_app/design/CustomDialog.dart';
import 'package:weather_app/utils/app_spacer.dart';
import 'package:weather_app/view/temperature.dart';
import 'package:weather_app/view/weather_list.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  GetData? fetchData;
  final GlobalKey _refreshkey = GlobalKey();
  RefreshController refreshController =
      RefreshController(initialRefresh: false);

  @override
  void initState() {
    fetchData = Provider.of<GetData>(context, listen: false);
    fetchData!.weather();
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black26,
      body: Consumer<GetData>(builder: (context, data, child) {
        return Container(
          decoration: const BoxDecoration(
              image: DecorationImage(
                  fit: BoxFit.cover, image: AssetImage("assets/bc.jpg"))),
          height: ScreenUtil().screenHeight,
          width: ScreenUtil().screenWidth,
          child: SmartRefresher(
            physics: const AlwaysScrollableScrollPhysics(),
            key: _refreshkey,
            controller: refreshController,
            enablePullDown: true,
            enablePullUp: true,
            header: const WaterDropMaterialHeader(
              backgroundColor: Colors.black,
              color: Colors.white,
            ),
            footer: const ClassicFooter(
              loadStyle: LoadStyle.ShowWhenLoading,
            ),
            onRefresh: _onRefresh,
            child: data.weatherModel != null
                ? ListView(
                    children: [
                      AppSpaces.spaces_height_40,
                      const Temperature(),
                      const WeatherList()
                    ],
                  )
                : Customdialog(),
          ),
        );
      }),
    );
  }

  Future<void> _onRefresh() async {
    await Future.delayed(Duration(seconds: 2));

    fetchData!.weather();

    setState(() {
      refreshController.refreshCompleted();
    });
  }
}
