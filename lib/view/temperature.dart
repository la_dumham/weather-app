import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:weather_app/controller/get_data.dart';
import 'package:weather_app/design/temp_card.dart';

class Temperature extends StatefulWidget {
  const Temperature({Key? key}) : super(key: key);

  @override
  _TemperatureState createState() => _TemperatureState();
}

class _TemperatureState extends State<Temperature> {
  @override
  Widget build(BuildContext context) {
    return Consumer<GetData>(builder: (context, data, child) {
      return data.weatherModel != null
          ? TempCard(
              temp: data.weatherModel!.main!.temp.toString(),
              city:
                  "${data.weatherModel!.name.toString()},${data.weatherModel!.sys!.country}",
            )
          : Container();
    });
  }
}
