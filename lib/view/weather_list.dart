import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart';
import 'package:weather_app/controller/get_data.dart';
import 'package:weather_app/design/weather_card.dart';

class WeatherList extends StatefulWidget {
  const WeatherList({Key? key}) : super(key: key);

  @override
  _WeatherListState createState() => _WeatherListState();
}

class _WeatherListState extends State<WeatherList> {
  @override
  Widget build(BuildContext context) {
    return Consumer<GetData>(builder: (context, data, child) {
      return data.weatherModel != null
          ? Padding(
              padding: EdgeInsets.only(left: 15.w, right: 15.w, top: 10.h),
              child: SizedBox(
                width: ScreenUtil().screenWidth,
                child: Column(
                  children: [
                    Row(
                      children: [
                        Expanded(
                            child: WeatherCard(
                          tittle: "Wind",
                          value: "${data.weatherModel!.wind!.speed}",
                        )),
                        Expanded(
                            child: WeatherCard(
                          tittle: "Feels Like",
                          value: "${data.weatherModel!.main!.feelsLike}°C",
                        )),
                      ],
                    ),
                    Row(
                      children: [
                        Expanded(
                            child: WeatherCard(
                          tittle: "Humidity",
                          value: "${data.weatherModel!.main!.humidity}",
                        )),
                        Expanded(
                            child: WeatherCard(
                          tittle: "Pressure",
                          value: "${data.weatherModel!.main!.pressure}",
                        )),
                      ],
                    ),
                    Row(
                      children: [
                        Expanded(
                            child: WeatherCard(
                          tittle: "Minimum Temperature",
                          value: "${data.weatherModel!.main!.tempMin}°C",
                        )),
                        Expanded(
                            child: WeatherCard(
                          tittle: "Minimum Temperature",
                          value: "${data.weatherModel!.main!.tempMax}°C",
                        )),
                      ],
                    ),
                  ],
                ),
              ),
            )
          : Container();
    });
  }
}
