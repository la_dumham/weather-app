import 'dart:convert';

import 'package:api_cache_manager/api_cache_manager.dart';
import 'package:api_cache_manager/models/cache_db_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:geolocator/geolocator.dart';
import 'package:weather_app/controller/base_controller.dart';
import 'package:weather_app/model/WeatherModel.dart';
import 'package:weather_app/utils/common_string.dart';
import 'package:weather_app/utils/service/base_client.dart';
import 'package:weather_app/utils/url.dart';

class GetData extends ChangeNotifier with BaseController {
  List? products = [];

  Position? position;
  WeatherModel? weatherModel;

  Future weather() async {
    var isCacheExist =
        await APICacheManager().isAPICacheKeyExist('weather_data');
    var position = await determinePosition();
    if (!isCacheExist) {
      final response = await BaseClient()
          .get(MyUrl.baseUrl,
              "lat=${position.latitude}&lon=${position.longitude}&appid=${MyText.apiKey}&units=metric")
          .catchError(handleError);

      var data = json.decode(response!);

      print(data.toString());

      weatherModel = WeatherModel.fromJson(data);

      APICacheDBModel cacheDBModel = APICacheDBModel(
        key: 'weather_data',
        syncData: response,
      );
      await APICacheManager().addCacheData(cacheDBModel);

      notifyListeners();
    } else {
      try {
        final response = await BaseClient()
            .get(MyUrl.baseUrl,
                "lat=${position.latitude}&lon=${position.longitude}&appid=${MyText.apiKey}&units=metric")
            .catchError(handleError);

        var data = json.decode(response!);

        print(data.toString());

        weatherModel = WeatherModel.fromJson(data);
        notifyListeners();
      } catch (e) {
        var cacheData = await APICacheManager().getCacheData('weather_data');

        var data = json.decode(cacheData.syncData);

        print("from local : " + data.toString());

        weatherModel = WeatherModel.fromJson(data);
        notifyListeners();
      }
    }
  }

  Future<Position> determinePosition() async {
    bool serviceEnabled;
    LocationPermission permission;

    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      return Future.error('Location services are disabled.');
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        return Future.error('Location permissions are denied');
      }
    }

    if (permission == LocationPermission.deniedForever) {
      // Permissions are denied forever, handle appropriately.
      return Future.error(
          'Location permissions are permanently denied, we cannot request permissions.');
    }

    return await Geolocator.getCurrentPosition();
  }
}
