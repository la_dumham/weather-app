import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:weather_app/utils/app_spacer.dart';
import 'package:weather_app/utils/color_me.dart';
import 'package:weather_app/utils/common_widget.dart';

class TempCard extends StatefulWidget {
  final String? temp;
  final String? city;

  const TempCard({Key? key, this.temp, this.city}) : super(key: key);

  @override
  _TempCardState createState() => _TempCardState();
}

class _TempCardState extends State<TempCard> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: 20.w, right: 20.w, top: 10.h),
      child: Container(
          decoration: BoxDecoration(
            color: Colors.black.withOpacity(0.3),
            borderRadius: const BorderRadius.only(
                topLeft: Radius.circular(10),
                topRight: Radius.circular(10),
                bottomLeft: Radius.circular(10),
                bottomRight: Radius.circular(10)),
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.4),
                spreadRadius: 0.5,
                blurRadius: 0.5,
                // changes position of shadow
              ),
            ],
          ),
          alignment: Alignment.center,
          width: ScreenUtil().screenWidth,
          child: Column(
            children: [
              AppSpaces.spaces_height_30,
              Icon(
                Icons.wb_sunny_rounded,
                color: Colors.yellow,
                size: 50.r,
              ),
              setCommonText("${widget.temp}°C", Color_me.of_white, 25.sp,
                  FontWeight.w500, 1),
              setCommonText(
                  "${widget.city}", Colors.white, 16.sp, FontWeight.w400, 1),
              AppSpaces.spaces_height_30,
            ],
          )),
    );
  }
}
