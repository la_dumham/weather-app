import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:weather_app/utils/app_spacer.dart';
import 'package:weather_app/utils/common_widget.dart';

class WeatherCard extends StatefulWidget {
  final String? tittle;
  final String? value;
  const WeatherCard({Key? key, this.tittle, this.value}) : super(key: key);

  @override
  _WeatherCardState createState() => _WeatherCardState();
}

class _WeatherCardState extends State<WeatherCard> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(5.r),
      child: Container(
        decoration: BoxDecoration(
          color: Colors.black.withOpacity(0.3),
          borderRadius: const BorderRadius.only(
              topLeft: Radius.circular(10),
              topRight: Radius.circular(10),
              bottomLeft: Radius.circular(10),
              bottomRight: Radius.circular(10)),
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.4),
              spreadRadius: 0.5,
              blurRadius: 0.5,
              // changes position of shadow
            ),
          ],
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            AppSpaces.spaces_height_10,
            setCommonText(
                widget.tittle!, Colors.white, 15.sp, FontWeight.w500, 2,
                talign: TextAlign.center),
            setCommonText(
                widget.value!, Colors.white, 15.sp, FontWeight.w400, 1),
            AppSpaces.spaces_height_10,
          ],
        ),
      ),
    );
  }
}
